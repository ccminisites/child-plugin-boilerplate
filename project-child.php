<?php
/*
Plugin Name: Project Child
Plugin URI: https://www.contentcoffee.com
Description: Child Project Plugin for CC Minisites/Blends
Author: Content and Coffee
Version: 1.0
*/

/*
Ok, I get it, plugins don't have parents... yes but we call this a child plugin so that is easier to understand
which plugin is doing what. project is the main plugin of blends, child project is the plugin specific to the project.

What goes in here?

Anything that is plugin related, not theme related, and specific to just this project.
*/
