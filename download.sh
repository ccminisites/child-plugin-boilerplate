# OK go get the repo as a zipo.
rm -rf project-child
wget -q https://gitlab.com/ccminisites/child-plugin-boilerplate/-/archive/master/child-plugin-boilerplate-master.zip
unzip child-plugin-boilerplate-master.zip
rm -rf child-plugin-boilerplate-master.zip
mv child-plugin-boilerplate-master project-child
rm project-child/download.sh
rm project-child/README.md
mv project-child/README-GENERIC.md project-child/README.md

