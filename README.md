# Child Plugin Boilerplate

This is the boiler plate for a child plugin to get your started. 

This is going to be where you make theme changes in each project in the context of a plugin.

**This script will download the child plugin boilerplate and put it into your project**

**it will overwrite wp-content/plugins/project-child**

**After you have done this once in the project, you should never do it again!**

After you have done this your project now has a project specific child plugin that should be committed in the repo of the project!

## RUN THIS ONCE IN WP-CONTENT/THEMES! ##

It will overwrite **wp-content/theme/project-child**

```bash <(wget -qO- https://gitlab.com/ccminisites/child-plugin-boilerplate/-/raw/master/download.sh)```

!!! You need to add to the project .gitignore !!!

```
!wp-content/plugins/project-child
!wp-content/plugins/project-child/*
!wp-content/plugins/project-child/*/
!wp-content/plugins/project-child/*/*
!wp-content/plugins/project-child/*/*/
!wp-content/plugins/project-child/*/*/*
```
